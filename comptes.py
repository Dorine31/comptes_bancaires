#!/bin/python3
from personne import Personne

class CompteSimple:
    ''' creation du compte simple d'un titulaire, avec possibilité de crediter ou de débiter le solde."
    '''
    #numeros_cptes = 10001

    #def __init__(self, titulaire, solde):  
        ''' si chaque compte gère directement la création des numéros de compte
        numeros_cptes: variable de classe => permet de partager les numéros de comptes entre tous les comptes
        '''            
    #    self.titulaire = titulaire
    #    self.__solde = solde
    #    self.__numero_cpte = CompteSimple.numeros_cptes
    #    CompteSimple.__numeros_cptes += 1

    def __init__(self, titulaire, solde, numero_cpte):
        ''' construction du compteSimple
            titulaire: string       => le titulaire passé en paramètre est le nom de la classe Personne
            __solde: float            => variable "privée", solde du compte 
            __numero_cpte: int          => variable "privée", numéro de compte donné par la banque
        '''
        self.titulaire = titulaire
        self.__solde = solde
        self.__numero_cpte = numero_cpte ##on ne peut pas non plus modifier le numero de compte

    def __eq__(self, autre): ##permet de travailler avec pytest, sinon impossible de vérifier une égalité
        return self.titulaire == autre.titulaire and self.__solde == autre.__solde and self.__numero_cpte == autre.__numero_compte
 
    @property
    ''' redéfinition du solde pour un accès en lecture '''
    def solde(self):
        return self.__solde

    @property
    ''' redéfinition du numero de compte pour un accès en lecture '''
    def numero_cpte(self):
        return self.__numero_cpte

    #@solde.setter
    #def solde(self, value):
    #    self.__solde = value

    #def _verifier_solde(self, argent):
    #    if self.__solde < argent :
    #        raise ValueError("pas assez d'argent sur le compte")

    def crediter(self, argent):
        ''' crediter un compte bancaire 
        solde: float
        argent: float   => somme que le titulaire crédite sur le compte
        '''
        self.__solde += argent

    def debiter(self, argent):
        ''' débiter un compte bancaire
        cf. crediter()
        '''
    #    self._verifier_solde(argent) ##pas utilisé ici car découvert autorisé
        self.__solde -= argent

    def __str__(self):
        return("{} possède {}€ sur son compte bancaire numéro {}.".format(self.titulaire, self.__solde, self.numero_cpte))

    def __repr__(self):
        return f"({repr(self.titulaire)}, {repr(self.solde)}, {repr(self.numero_cpte)})"

    def __iadd__(self, argent):##le iadd est pertinent car on ajoute une valeur à notre compte
        self.__solde += argent
        return self

    def __isub__(self, argent):
    #    verifier_solde(argent)
        self.__solde -= argent
        return self

class CompteCourant(CompteSimple):
    '''creation du compte courant d'un client, ayant la même utilisation qu'un compte simple et la possibilité d'éditer un relevé bancaire
    historique: liste       => initialisée dans le compte. dépendance de la Classe historique
    '''
    def __init__(self, titulaire, solde, numero_cpte):
        super().__init__(titulaire, solde, numero_cpte)
        self.historique = [] ##on ne passe pas l'historique en paramètre car il est créé

    def __eq__(self, autre): ##permet de travailler avec pytest, sinon impossible de vérifier une égalité
        return self.titulaire == autre.titulaire and self.__solde == autre.__solde and self.__numero_cpte == autre._numero_compte and self.historique == autre.historique
    
    def _add_to_historique(self, operation, argent): ##méthode locale 
        ''' ajouter les opérations de débit et de crédit à l'historique du compte courant 
        operation: string       => credit ou debit 
        argent: float           => somme débitée ou créditée
        '''
        self.historique.append([operation, argent])

    def crediter(self, argent):
        ''' crediter une somme sur le compte
        argent: float   => somme créditée
        '''
        super().crediter(argent)
        self._add_to_historique("credit", argent)

    def debiter(self, argent):
        ''' débiter une somme sur le compte
        argent: float   => somme débitée
        '''
        super().debiter(argent)
        self._add_to_historique("debit", argent)

    def afficher_releve(self):
        ''' afficher le relevé bancaire du compte '''
        print("relevé bancaire")
        for operation in self.historique:
            print(f"{operation[0]}, {operation[1]}€")
        print(f"solde final : {self.solde}€")

    def __str__(self):
        return(super().__str__() + "voici l'historique {}.".format(self.historique))

class Historique:
    '''creation d'un historique, permettant d'éditer un relevé bancaire du compte courant d'un client
    historique: liste       => initialisation de l'historique à la création du compte 
    '''
    def __init__(self):
        self.historique = []

    def __str__(self):
        return f"historique : {self.historique}"

    def __repr__(self):
        return f"historique: {repr(self.historique)}"
