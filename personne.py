 #!/bin/python3

class Personne:
    '''
    creation d'une classe Personne, le titulaire du compte bancaire
    '''
    def __init__(self, nom):
        '''construction de la personne
        nom: string     => le nom de la personne, et donc du titulaire du compte
        '''
        self.nom = nom
    
    def __eq__(self, autre):
        return self.nom == autre.nom

    def __repr__(self):
         return "Personne({})".format(repr(self.nom))

    def __str__(self):
        return "Personne{}".format(self.nom)
