#!/bin/python3

from comptes import CompteSimple
from comptes import CompteCourant
from comptes import Historique
from personne import Personne


class Banque:
    ''' une banque qui gère des comptes bancaires, simples ou courants et qui permet d'ouvrir un compte,/
     de prélever des frais bancaires et de calculer le solde de tous les comptes
        comptes: liste      => liste de tous les comptes ouverts à la banque
        dernier_numero: int => dernier numéro attribué à un compte
    '''
    ## ici on décrit les attributs d'instance (comptes, dernier_numero)
    
    def __init__(self):
        ''' initialisation de la banque 
        '''
        ## ici on décrit les paramètres de init
        self.comptes = []
        self.dernier_numero = 10000

    def __eq__(self, autre): ##permet de travailler avec pytest, sinon impossible de vérifier une égalité
        return self.comptes == autre.comptes and self.dernier_numero == autre.dernier_numero

    def ouvrir_compteSimple(self, titulaire, depot_initial):
        '''  creation d'une fonction ouvrir_compte pour les comptes simples 
        dernier_numero: int         => incrémentation du numéro pour attribution auto à un nouveau compte
        compte: classe Compte       => chaque compte possède un titulaire et un solde
        titulaire: string           => titulaire du compte bancaire
        depot_initial: float        => somme déposée à l'ouverture du compte
        comptes: liste              => ajout du compte dans la banque
        '''
        self.dernier_numero += 1
        compte = CompteSimple(titulaire, depot_initial, self.dernier_numero)
        self.comptes.append(compte)
        return compte

    def ouvrir_compteCourant(self, titulaire, depot_initial):
        ''' creation d'une fonction ouvrir_compte pour les comptes courants, en y ajoutant l'historique
        var : cf. ouvrir_compteSimple()
         '''
        self.dernier_numero += 1
        compte = CompteCourant(titulaire, depot_initial, self.dernier_numero)
        self.comptes.append(compte)
        return compte

    def prelever_frais(self, n):
        ''' prelever des frais bancaires à tous les comptes
        var:
            compte: liste       => chaque compte de la banque
            comptes: liste      => banque
            frais: float        => frais débités du compte
            n: entier           => valeur donnée par la banque
        '''
        for compte in self.comptes:
            frais = (compte.solde *n)
            compte.debiter(frais)

    def calculer_solde(self):
        ''' calculer le cumul de tous les comptes bancaires 
        somme_comptes: float        => somme de tous les comptes
        '''
        somme_comptes = 0
        for compte in self.comptes:
            somme_comptes += compte.solde
        return somme_comptes

    def editer_releves(self):
        ''' éditer le relevé de tous les comptes courants
        '''
        for self.compte in self.comptes:
            if isinstance(self.compte, CompteCourant):
                self.compte.afficher_releve()

    def __repr__(self):
        return "banque : {}".format(self.comptes)

    def __str__(self):
        return "Comptes :\n - {}.".format('\n - '.join(str(c) for c in self.comptes)) ##quand on fait le str d'une liste (bq), cela n'affiche pas le str des éléments de la liste (compte). => faire un join pour chaque élément

if __name__ == "__main__":
    bq = Banque()
    bq.ouvrir_compteSimple("dorine", 50)
    print(bq)
    bq.ouvrir_compteCourant("ludo", 20)
    print(bq)
    bq.prelever_frais(0.03)
    bq.editer_releves()
    print(bq.comptes)
    cc = CompteCourant("natacha", 10, 10003)
    print(cc)
    l=[cc]
    print(l)

