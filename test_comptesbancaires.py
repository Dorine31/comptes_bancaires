#!/bin/python3

import pytest

from banque import Banque
from comptes import CompteSimple
from comptes import CompteCourant
from comptes import Historique
from personne import Personne

@pytest.fixture
def p1(): #initialisation personne1
    return Personne('dorine')

@pytest.fixture
def p2():
    return Personne('ludo')

@pytest.fixture
def c1(p1): #initialisation compte1
    return CompteSimple(p1, 100., 10001)

@pytest.fixture
def c2(p2):
    return CompteSimple(p2, 542.2, 10002)

@pytest.fixture
def bp(): #initialistion banque1
    return Banque()

@pytest.fixture
def cc():
    return CompteCourant('natacha', 40, 10003, [])

@pytest.fixture
def h():
    return Historique()

def test__init__personne(p1, p2): #initialisation Personne
    assert p1.nom == 'dorine'
    assert p2.nom == 'ludo'

def test__init__compte(p1, c1, c2): #initialisation Compte
    assert c1.titulaire == Personne('dorine')
    assert c1.solde == 100.0
    assert c2.titulaire == Personne('ludo')
    assert c2.solde == 542.2

def test__init__banque(bp): #initialisation Banque
    assert bp.comptes == []

def test_init_historique(h):
    assert h.historique == []

def test_crediter(c1):
    c1.crediter(25)
    assert c1.solde == 125

def test_debiter(c2):
    #c2.debiter(804)
    #with pytest.raises(ValueError):
    #    c2.debiter(804)
    c2.debiter(84)
    assert c2.solde == 458.20000000000005

def test_ouvrir_compte(bp):
    c10 = CompteSimple("dorine", 100., 10004)
    c11 = CompteSimple("ludo", 41, 10005)
    c12 = CompteCourant("fred", 10, 10006, [])
    bp.comptes.append(["dorine", 100, 10004])
    bp.comptes.append(["ludo", 41, 10005])
    bp.comptes.append(["fred", 10, 10006, []])
    assert bp.comptes == [['dorine',100., 10004], ['ludo',41, 10005], ["fred", 10, 10006, []]]

def test_prelever_frais(bp):
    for compte in bp.comptes:
        assert compte.prelever_frais(0.03) == [['dorine', 97], ['ludo', 40.59]]

def test_calculer_solde(bp):
    for compte in bp.comptes:
        assert compte.calculer_solde() == 137.59

def test_init_compteCourant(cc):
    cc = ['nathalie', 40, 10007, []]

def test_add_to_historique(cc):
    cc.add_to_historique("credit", 25)
    assert cc.historique == [['credit', 25]]

def test_editer_releve(bp):
    bp.editer_releves()
